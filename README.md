# DocumentationEvolution
Raw results for the Computer Science bachelor thesis "Documentation Evolution" from Linnaeus University

- Student: Helena Tevar Hernandez
- Supervisor: Francis Palma
- Coordinator: Diego Perez and Jesper Andersson

The report can be found in: to be added

## Folders description
Each project  includes two directories: 
1. Releases Raw Data
2. Calculations

###  Releases Raw Data
This directory includes the data extracted from the source code of 10 consecutive releases from the parent project directory. 
The order of the releases is from the oldest (number 0) to the newest release (number 9).
It includes all the code blocks by type (Class or method), lines of code, Jaccard similarity ratio, Cosine similarity ratio, contents and comments parsed by a natural language process. 

### Calculations
This directory includes the results of the analysis done to each release raw data. 
- Average Size: Calculations of the average size and percentiles to be used for discrete divisions of the raw data. 
- Documented Calculated: Analysis of the number of blocks that had or missed documentation. It includes information of how many code blocks were added to a release, and if they had or missed documentation.
- Final: Variance ratio from the similarity ratios (Jaccard / Cosine):
  - by release
  - by percentile
  - by code block type (Class / Method)

## Projects studied
| Project   |      Repository      |  
|----------|:-------------:|
| Apache Maven          |  https://github.com/apache/Maven                |
| Apache Jmeter         |    https://github.com/apache/jmeter             |
| Eclipse Che           | https://github.com/eclipse/che                  |
| Apache Tomcat         | https://github.com/apache/tomcat                |
| Springboot - Spring   | https://github.com/spring-projects/spring-boot  |
| Apache CXF            | https://github.com/apache/cxf                   |
| Google Guava          |https://github.com/google/guava                  |
| Oracle Graal          | https://github.com/oracle/graal                 |
| Elastic ElasticSearch |  https://github.com/elastic/elasticsearch       |
| Apache Netbeans       |https://github.com/apache/netbeans               |
