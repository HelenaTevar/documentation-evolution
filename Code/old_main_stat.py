import matplotlib.pyplot as plt
import numpy as np
import sys
import csv
import os
from lib import cet_repository as repository
from lib import cet_statistics
from lib import cet_statistics_cohesion


def run_loc(args, case):
    projects = repository.get_projects_packaged(args)
    for project in projects:
        s = cet_statistics.cet_statistics()
        s.run(project, case)


def run_cohesion(args):
    c = cet_statistics_cohesion.statistics_cohesion()
    c.run(args)


# run(sys.argv[1])
#run_loc('maven', 0)
run_cohesion('maven')
