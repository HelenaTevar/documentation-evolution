from lib import cet_repository as repository_module
from lib import cet_scratcher as extractor
import sys

'''
Requires a  projects.csv database in lib to work. 
Returns a group of csv files for each release.

run: Python3 main.py PROJECT_NAME 
'''

#releases = repository_module.get_project('test')
#releases = repository_module.get_project(sys.argv[1])


def run(arg='test'):
    releases = repository_module.get_project(arg)
    repository_module.set_output(releases)
    for release in releases:
        print('>> Main: Running - ', release['release_name'])
        scratcher = extractor.cet_scratcher(release)
        scratcher.run()
        repository_module.asd = True
        print('Main: Finished - ', release['release_name'])
    print('Main: FINISHED - Please check the output folder to get the results')


try:
    run(sys.argv[1])
except:
    run()
