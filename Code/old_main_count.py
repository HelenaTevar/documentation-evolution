from lib import cet_repository as repository
from lib import cet_calc as m_calculator
import sys
import csv
import os
'''
Counts how many methods and classes are documented or not, 
counts how many methods and classes are new additions or old ones.
'''


def run(args):
    print('Running with ', args)
    calculator = m_calculator.calc(args)
    calculator.run()
    print('Process finished')


run('maven')
