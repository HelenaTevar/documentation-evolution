import csv
import os
releases_fields = ['project_name',
                   'release_name', 'release_date', 'in', 'out']


def get_projects():
    '''
    Get all the projects from lib/projects.csv
    '''
    results = [fields for fields in csv.DictReader(
        open('lib/projects.csv', newline=''))]
    return results


def get_project(project_name):
    '''
    Filters the list of projects to get the parameter project.

    If the parameter is "all", returns all the projects
    '''
    if project_name == 'all':
        return get_projects()
    else:
        all_releases = [fields for fields in csv.DictReader(
            open('lib/projects.csv', newline=''))]

        def release_filter(iterable):
            if(iterable['project_name'] == project_name):
                return True
            else:
                return False
        result = filter(release_filter, all_releases)
    return list(result)


def get_projects_packaged(project_name):
    '''
    Returns a list of list of projects. Example:

    [[Project 1 releases],[Project 2 releases]]
    '''
    all_projects = get_project(project_name)
    names = set()
    for release in all_projects:
        names.add(release['project_name'])
    result = list()
    for name in names:
        single_project = get_project(name)
        result.append(single_project)
    return result


def get_db(path):
    '''
    Get the dictionary from the path.csv file
    '''
    results = [fields for fields in csv.DictReader(
        open(path, newline=''))]
    return results


def set_output(projects_list):
    '''
    Creates directories for the projects output
    '''
    for release in projects_list:
        m_path = 'output/' + release['project_name']
        if not os.path.isdir(m_path):
            os.mkdir(m_path)
    print('Repo: Checking output directories - ', m_path)


def save_results(repo, result_dict):
    '''
    Saves the results from the main run -> cet_tool.save
    '''
    is_first_save = not os.path.exists(repo['out'])

    blocks_fields = ['project_name', 'release_name', 'release_date',
                     'identifier', 'type', 'loc', 'owner', 'jaccard', 'cosine', 'comment', 'content']
    writter = csv.DictWriter(open(repo['out'], 'a'), fieldnames=blocks_fields)

    if is_first_save:
        writter.writeheader()

    writter.writerows(result_dict)
    del writter


def save_db(path, data, m_fields):
    '''
    Saves data (list of dic)

    in the path (csv file)

    with fieldnames = m_fields

    '''
    is_first_save = not os.path.exists(path)

    blocks_fields = m_fields
    writter = csv.DictWriter(open(path, 'a'), fieldnames=blocks_fields)

    if is_first_save:
        writter.writeheader()

    writter.writerows(data)
    del writter
