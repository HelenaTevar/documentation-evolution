from lib import cet_repository


class calc_history(object):
    def __init__(self, project):
        super().__init__()
        self.releases = project
        self.path = 'output/calculations/history_'
        self.history_fields = ['project_name', 'identifier', 'type', 'owner', 'r:0_loc',
                               'r:0_date', 'r:0_jaccard', 'r:0_cosine', 'r:1_loc', 'r:1_date',
                               'r:1_jaccard', 'r:1_cosine', 'r:2_loc', 'r:2_date', 'r:2_jaccard',
                               'r:2_cosine', 'r:3_loc', 'r:3_date', 'r:3_jaccard',
                               'r:3_cosine', 'r:4_loc', 'r:4_date', 'r:4_jaccard',
                               'r:4_cosine', 'r:5_loc', 'r:5_date', 'r:5_jaccard', 'r:5_cosine', 'r:6_loc',
                               'r:6_date', 'r:6_jaccard', 'r:6_cosine', 'r:7_loc', 'r:7_date', 'r:7_jaccard',
                               'r:7_cosine', 'r:8_loc', 'r:8_date', 'r:8_jaccard', 'r:8_cosine', 'r:9_loc',
                               'r:9_date', 'r:9_jaccard', 'r:9_cosine']
        self.current = list()
        self.history = list()
        self.index_release = 0

    def save_db(self):
        cet_repository.save_db(self.path, self.history, self.history_fields)

    def run(self):
        self.path += self.releases[0]['project_name'] + '.csv'
        while self.index_release < len(self.releases):
            print('Calc history release ', self.index_release)
            self.calculate(self.releases[self.index_release])
            self.index_release += 1
        self.save_db()

    def calculate(self, release):
        data = cet_repository.get_db(release['out'])
        for block in data:
            if not self.is_old(block):
                self.is_new(block)
        self.save_release()

    def save_release(self):
        self.history.extend(self.current)
        self.current.clear()

    def is_new(self, block):
        result = {'project_name': block['project_name'],
                  'identifier': block['identifier'],
                  'type': block['type'],
                  'owner': block['owner']}
        for i in range(0, 10):
            key = 'r:' + str(i) + '_loc'
            result[key] = ''
            key = 'r:' + str(i) + '_date'
            result[key] = ''
            key = 'r:' + str(i) + '_jaccard'
            result[key] = ''
            key = 'r:' + str(i) + '_cosine'
            result[key] = ''

        key = 'r:' + str(self.index_release) + '_loc'
        result[key] = block['loc']
        key = 'r:' + str(self.index_release) + '_date'
        result[key] = block['release_date']
        key = 'r:' + str(self.index_release) + '_jaccard'
        result[key] = block['jaccard']
        key = 'r:' + str(self.index_release) + '_cosine'
        result[key] = block['cosine']
        self.current.append(result)

    def is_old(self, block):
        for b in self.history:
            if self.are_same_block(b, block):
                key = 'r:' + str(self.index_release) + '_loc'
                b[key] = block['loc']
                key = 'r:' + str(self.index_release) + '_date'
                b[key] = block['release_date']
                key = 'r:' + str(self.index_release) + '_jaccard'
                b[key] = block['jaccard']
                key = 'r:' + str(self.index_release) + '_cosine'
                b[key] = block['cosine']

                self.current.append(b)
                self.history.remove(b)
                return True
        return False

    def are_same_block(self, block1, block2):
        if block1['identifier'] == block2['identifier'] and block1['owner'] == block2['owner'] and block1['type'] == block2['type']:
            return True
        else:
            return False
