class cet_block():
    def __init__(self, release):
        super().__init__()
        self.loc = 0
        self.depth = int()
        self.active = False
        self.identifier = None
        self.type = ''
        self.comments = list()
        self.comments_filtered = ''
        self.block_content = list()
        self.content_filtered = ''
        self.content = ''
        self.project_name = release['project_name']
        self.release_name = release['release_name']
        self.release_date = release['release_date']
        self.owner = None
        self.jaccard = 0.0
        self.cosine = 0.0

    def is_empty(self):
        '''
        @return: boolean if identifiers is None
        '''
        return self.identifier is None

    def to_dict(self):
        '''
        @return: Dictionary with block data
        '''
        result = {'project_name': self.project_name,
                  'release_name': self.release_name,
                  'release_date': self.release_date,
                  'identifier': self.identifier,
                  'type': self.type,
                  'loc': self.loc,
                  'owner': self.owner,
                  'jaccard': self.jaccard,
                  'cosine': self.cosine,
                  'comment': self.comments_filtered,
                  'content': self.content_filtered}
        return result

    def print(self):
        print('**Block id: ', self.identifier, ' - owner: ', self.owner)
        print('\tType: ', self.type, ' - LOC: ',
              self.loc, ' - depth: ', self.depth)
        print('\tComments: ', len(self.comments))

    def print_verbose(self):
        '''
        @return: Print information of block, including all the comments and contents.
        '''
        print('**Block id: ', self.identifier, ' - owner: ', self.owner)
        print('\tType: ', self.type, ' - LOC: ',
              self.loc, ' - depth: ', self.depth)
        print('\tProject: ', self.project_name, ' - Release; ',
              self.release_name, ' - Date: ', self.release_date)
        print('\tComment: ', self.comments_filtered)
        print('\tContent: ', self.content_filtered)
        print('\tJaccard: ', self.jaccard, ' - Cosine: ', self.cosine)
        print('\t---RAW COMMENT')
        for c in self.comments:
            print(c)
        print('\t---RAW CONTENT')
        for c in self.block_content:
            print(c)
        print('***********')

    def has_depth(self):
        if (self.depth != None):
            return True
        return False

    def is_active(self):
        return self.active

    def set_block(self, new_depth, new_id, new_owner):
        self.depth = new_depth
        self.identifier = new_id
        self.owner = new_owner

    def set_depth(self, new_depth):
        self.depth = new_depth

    def set_class(self, new_depth, new_id):
        '''
        new_depth, new_id
        '''
        self.depth = new_depth
        self.identifier = new_id
        self.owner = new_id
        self.active = True

    def set_method(self, new_depth, new_id):
        '''
        new_depth, new_id
        '''
        self.depth = new_depth
        self.identifier = new_id
        self.active = True

    def set_active(self):
        self.active = True

    def set_inactive(self, depth_level):
        if self.depth != None and self.depth >= depth_level:
            self.active = False

    def set_project(self, project_name, release_name, release_date):
        self.project_name = project_name
        self.release_name = release_name
        self.release_date = release_date
