from lib import cet_repository
import sys
import csv


class cet_calc_doc:
    def __init__(self, releases):
        super().__init__()
        self.current_release = {'release_name': '',
                                'release_date': '',
                                'new_doc_class': 0,
                                'new_doc_method': 0,
                                'new_not_class': 0,
                                'new_not_method': 0,
                                'old_doc_class': 0,
                                'old_doc_method': 0,
                                'old_not_class': 0,
                                'old_not_method': 0,
                                'total': 0}
        self.releases = releases
        self.path = 'output/calculations/documented_'
        self.history = list()
        self.current = list()

    def run(self):
        self.path += self.releases[0]['project_name'] + '.csv'

        for release in self.releases:
            self.calculate(release)

    def calculate(self, release):
        data = cet_repository.get_db(release['out'])
        for block in data:
            if not self.is_found(block):
                self.is_new(block)
            self.current_release['total'] += 1
        self.save()

        self.current_to_history()
        self.reset_values()

        # reset current release and current list
    def reset_values(self):
        self.current.clear()
        self.current_release.clear()
        self.current_release = {'release_name': '',
                                'release_date': '',
                                'new_doc_class': 0,
                                'new_doc_method': 0,
                                'new_not_class': 0,
                                'new_not_method': 0,
                                'old_doc_class': 0,
                                'old_doc_method': 0,
                                'old_not_class': 0,
                                'old_not_method': 0,
                                'total': 0}

    def current_to_history(self):
        self.history.extend(self.current)

    def save(self):
        print(self.current_release)

    def is_new(self, block):
        self.current.append(block)
        if block['type'] == 'class':
            if block['comments']:
                self.current_release['new_doc_class'] += 1
            else:
                self.current_release['new_not_class'] += 1
        else:
            if block['comments']:
                self.current_release['new_doc_method'] += 1
            else:
                self.current_release['new_not_method'] += 1

    def is_found(self, block):
        for b in self.history:
            if self.are_same_block(b, block):
                self.current.append(b)
                self.history.remove(b)
                self.is_old_block(block)
                return True
        return False

    def is_old_block(self, block):
        if block['type'] == 'class':
            if block['comments']:
                self.current_release['old_doc_class'] += 1
            else:
                self.current_release['old_not_class'] += 1
        else:
            if block['comments']:
                self.current_release['old_doc_method'] += 1
            else:
                self.current_release['old_not_method'] += 1

    def are_same_block(self, block1, block2):
        if block1['identifier'] == block2['identifier'] and block1['owner'] == block2['owner'] and block1['type'] == block2['type']:
            return True
        else:
            return False
