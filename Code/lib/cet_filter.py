import re
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
import lib.cet_regex as cet_regex

lemmatizer = WordNetLemmatizer()

WS = re.compile(cet_regex.get_whitespace_regex())
COMMENT = re.compile(cet_regex.get_filter_comment())
JAVA_KEYWORDS = re.compile(cet_regex.get_filter_java_keywords())
JAVA_KEYSYMBOLS = re.compile(cet_regex.get_filter_java_keysymbols())
WORD = re.compile(cet_regex.get_filter_word())
NUM = re.compile(cet_regex.get_numbers_regex())
PRONOMS = re.compile(cet_regex.get_pronoms())
PREPO = re.compile(cet_regex.get_prepositions())
SEPARATORS = re.compile(cet_regex.get_separators())


def camel_case_split(identifier):
    matches = re.finditer(
        cet_regex.get_naming_conventions_regex(), identifier)
    split_string = []
    # index of beginning of slice
    previous = 0
    for match in matches:
        # get slice
        split_string.append(identifier[previous:match.start()])
        # advance index
        previous = match.start()
    # get remaining string
    split_string.append(identifier[previous:])
    return split_string


def parse_naming_conventions(line):
    words = create_list_words(line)
    naming_parsed = []
    for w in words:
        uncameled = camel_case_split(w)
        naming_parsed.extend(uncameled)

    final = join_list_words(naming_parsed)
    return final


def lemmatize(line):
    words = create_list_words(line)
    result = []
    for w in words:
        lematized = lemmatizer.lemmatize(w)
        result.append(lematized)
    return join_list_words(result)


def join_list_words(list_words):
    separator = ' '
    return separator.join(list_words)


def create_list_words(line):
    return WORD.findall(line)


def stop_words(line):
    stop_words = set(stopwords.words('english'))
    for w in stop_words:
        word = ' ' + w + ' '
        line = line.replace(word, ' ')
    return line


def filter_WS(line):
    filtered_line = WS.sub(' ', line)
    return filtered_line


def extract_java_keywords(line):
    filtered = JAVA_KEYSYMBOLS.sub(' ', line)
    filtered = JAVA_KEYWORDS.sub(' ', filtered)
    return filtered


def remove_comment_tags(line):
    return COMMENT.sub(' ', line)


def natural_language_filter(line):
    filtered = PRONOMS.sub(' ', line)
    filtered = filtered.lower()
    filtered = PREPO.sub(' ', filtered)
    filtered = NUM.sub(' ', filtered)
    filtered = stop_words(filtered)
    filtered = lemmatize(filtered)
    return filtered


def fix_white_spaces(line):
    filtered = filter_WS(line)
    return filtered.strip()


def general_filter(line):
    filtered = line
    filtered = filtered.replace('\n', ' ')

    filtered = remove_comment_tags(filtered)

    filtered = extract_java_keywords(filtered)
    filtered = filtered.replace('@', ' ')
    filtered = SEPARATORS.sub(' ', filtered)
    filtered = parse_naming_conventions(filtered)
    filtered = natural_language_filter(filtered)

    return fix_white_spaces(filtered)


def filter_comment(line):
    return general_filter(line)


def filter_code(line):
    return general_filter(line)
