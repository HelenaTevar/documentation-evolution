import re


class regex:
    def __init__(self):
        super().__init__()
        self.regex_ws = re.compile('\s*|')
        self.regex_comment = re.compile(
            '^\s*((\*\s*/)+|(/\s*\*)|(/\s*\*\s*\*)|(\*\s*)|(/\s*/)).*')
        self.regex_comment_block = re.compile('^(\s*)(\*)|(/\*\s*\*)|(/\s*\*)')
        self.regex_method = re.compile(
            '^\s*([a-zA-Z0-9,._<>]+\s+)*(?P<id>[a-zA-Z0-9_<>]+\s*\((\s*[a-zA-Z0-9?@()\"_\[\]<>,.]+)*\s*\))(.*)')
        self.regex_possible_method = re.compile('\s*(.*\w+.*)(\(.*\)).*')
        # self.regex_conditionals = re.compile(
        #    cet_regex.get_conditionals_regex())

        self.regex_class = re.compile('^\s*(Annotation\s*|public\s*|protected\s*|private\s*|static\s*|abstract\s*|final\s*|native\s*|synchronized\s*|transient\s*|volatile\s*|strictfp\s*)*' +
                                      '(class|interface|enum)+' + '\s*(?P<id>[a-zA-Z_0-9]+)')
        self.regex_open_bracket = re.compile('{')
        self.regex_closed_bracket = re.compile('}')
        self.regex_closed_comment = re.compile('\*/|//')

        self.rsymbols = re.compile('(^\s*(\(|\)|\{|\}|<|>|@|\||\*|!|\/|\.).*)')
        self.rcondi = re.compile(
            '(^\s*(throw|super|synchronized|default|case|return|if|else|while|for|do|try|catch|switch)(.*|\s*)+)')
        self.rpat = re.compile('.*(=|->|;$).*')

    def has_open_bracket(self, line):
        return '{' in line

    def has_closed_bracket(self, line):
        return '}' in line

    def has_closed_comment(self, line):
        return '*/' in line or '//' in line

    def check_method_patterns(self, line):
        '''
        Symbols: = -> and ;
        '''
        eq = '=' in line
        lambd = '->' in line
        semi = ';' in line
        return eq or lambd or semi

    def check_method_keywords(self, line):
        keywords = ['throw', 'super', 'synchronized', 'default', 'case',
                    'return', 'if', 'else', 'while', 'for', 'do', 'try', 'catch', 'switch']
        for w in keywords:
            if w in line:
                return True
        return False

    def check_method_symbols(self, line):
        return self.rsymbols.search(line)

    def check_possible_method(self, line):
        return '(' in line or ')' in line

    def check_is_class(self, line):
        c = 'class' in line
        i = 'interface' in line
        e = 'enum' in line
        return c or i or e
