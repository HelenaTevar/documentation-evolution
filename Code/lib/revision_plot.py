import numpy as np
import lib.cet_repository as cet_repository
import matplotlib.pyplot as plt


class revision_plot():
    def __init__(self, project):
        super().__init__()
        self.project = project
        self.jaccard_class = list()
        self.jaccard_method = list()
        self.cosine_class = list()
        self.cosine_method = list()

    def run(self):
        self.data_list = self.divide_csv_to_lists()
        # self.plot_all()

    def plot_jaccard_class(self):
        self.plot(self.jaccard_class)

    def plot_all(self):
        self.plot(self.jaccard_class)
        self.plot(self.jaccard_method)
        self.plot(self.cosine_class)
        self.plot(self.cosine_method)

    def divide_csv_to_lists(self):
        path = 'output/calculations/variation_' + self.project+'.csv'
        data_list = cet_repository.get_db(path)

        jaccard_class = list()
        jaccard_method = list()
        cosine_class = list()
        cosine_method = list()

        roundBy = 50

        for data in data_list:
            title = self.do_title(data)
            result_dic = {'label': data['percentile'],
                          'results': list(),
                          'title': title}

            result_dic['results'].append(round(float(data['r:0']), roundBy))

            result_dic['results'].append(round(float(data['r:1']), roundBy))

            result_dic['results'].append(round(float(data['r:2']), roundBy))

            result_dic['results'].append(round(float(data['r:3']), roundBy))

            result_dic['results'].append(round(float(data['r:4']), roundBy))

            result_dic['results'].append(round(float(data['r:5']), roundBy))

            result_dic['results'].append(round(float(data['r:6']), roundBy))

            result_dic['results'].append(round(float(data['r:7']), roundBy))

            result_dic['results'].append(round(float(data['r:8']), roundBy))

            result_dic['results'].append(round(float(data['r:9']), roundBy))

            result_dic['ylabel'] = 'Variation'

            self.save_to_ratio_type_array(
                data['cohesion_type'], data['type'], result_dic)
            return data_list

    def save_to_ratio_type_array(self, cohesion, _type, dic):
        if cohesion == 'jaccard':
            if _type == 'class':
                self.jaccard_class.append(dic)
            else:
                self.jaccard_method.append(dic)
        else:
            if _type == 'class':
                self.cosine_class.append(dic)
            else:
                self.cosine_method.append(dic)

    def do_title(self, data):
        title = 'Project ' + self.project + ': ' + \
            data['cohesion_type'] + ' variation for '
        if data['type'] == 'class':
            title += 'classes'
        elif data['type'] == 'method':
            title += 'methods'
        return title

    def plot(self, dict_list):
        x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
        a = dict_list[0]['results']
        b = dict_list[1]['results']
        c = dict_list[2]['results']
        d = dict_list[3]['results']
        # fig, ax = plt.subplots()  # Create a figure and an axes.
        fig, ax = plt.subplots(figsize=(5, 5))
        # Plot some data on the axes.
        # ax.plot([0, 9], [1, 1], color='silver',
        #        linestyle=':', linewidth=1)
        ax.plot(x, a, label=dict_list[0]['label'])
        ax.plot(x, b, label=dict_list[1]['label'])
        ax.plot(x, c, label=dict_list[2]['label'])
        ax.plot(x, d, label=dict_list[3]['label'])
        plt.xticks(x)
        ax.set_xlabel('Releases')  # Add an x-label to the axes.
        ax.set_ylabel(dict_list[3]['ylabel'])
        # Add a title to the axes.
        ax.set_title(dict_list[3]['title'])
        # Add a legend.
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])

        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), )
        plt.subplots_adjust(left=0.145,
                            bottom=0.110,
                            right=0.820,
                            top=0.925,
                            wspace=0.2,
                            hspace=0.2)
        plt.grid()
        plt.show()

    def average_ratios(self, ):
        jaccard_class = list()
        jaccard_method = list()
        cosine_class = list()
        cosine_method = list()

        for data in self.data_list:
            if data['type'] == 'class':
                if data['cohesion_type'] == 'jaccard':
                    jaccard_class.append(data['avg'])
                else:
                    cosine_class.append(data['avg'])
            else:
                if data['cohesion_type'] == 'jaccard':
                    cosine_class.append(data['avg'])
                else:
                    cosine_method.append(data['avg'])

        result = {
            'project': self.project,
            'jaccard_class': self.average(jaccard_class),
            'jaccard_method': self.average(jaccard_method),
            'cosine_class': self.average(cosine_class),
            'cosine_method': self.average(cosine_method)
        }
        return result

    def average(self, m_list):
        avg = 0.0
        for n in m_list:
            avg = avg + float(n)
        return round(avg/4, 5)
