from lib import cet_repository
import csv
import sys


class statistics_cohesion:
    def __init__(self):
        super().__init__()
        self.project_name = ''
        self.class_avgs = dict()
        self.method_avgs = dict()
        self.keys = [['r:0_loc', 'r:0_date', 'r:0_jaccard', 'r:0_cosine'],
                     ['r:1_loc', 'r:1_date', 'r:1_jaccard', 'r:1_cosine'],
                     ['r:2_loc', 'r:2_date', 'r:2_jaccard', 'r:2_cosine'],
                     ['r:3_loc', 'r:3_date', 'r:3_jaccard', 'r:3_cosine'],
                     ['r:4_loc', 'r:4_date', 'r:4_jaccard', 'r:4_cosine'],
                     ['r:5_loc', 'r:5_date', 'r:5_jaccard', 'r:5_cosine'],
                     ['r:6_loc', 'r:6_date', 'r:6_jaccard', 'r:6_cosine'],
                     ['r:7_loc', 'r:7_date', 'r:7_jaccard', 'r:7_cosine'],
                     ['r:8_loc', 'r:8_date', 'r:8_jaccard', 'r:8_cosine'],
                     ['r:9_loc', 'r:9_date', 'r:9_jaccard', 'r:9_cosine']]
        self.jaccard_xs_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_s_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_m_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_l_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_xs_class_tt = list()
        self.jaccard_s_class_tt = list()
        self.jaccard_m_class_tt = list()
        self.jaccard_l_class_tt = list()

        self.cosine_xs_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_s_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_m_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_l_class = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_xs_class_tt = list()
        self.cosine_s_class_tt = list()
        self.cosine_m_class_tt = list()
        self.cosine_l_class_tt = list()

        self.jaccard_xs_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_s_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_m_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_l_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.jaccard_xs_method_tt = list()
        self.jaccard_s_method_tt = list()
        self.jaccard_m_method_tt = list()
        self.jaccard_l_method_tt = list()

        self.cosine_xs_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_s_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_m_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_l_method = {0: list(), 1: list(), 2: list(), 3: list(
        ), 4: list(), 5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        self.cosine_xs_method_tt = list()
        self.cosine_s_method_tt = list()
        self.cosine_m_method_tt = list()
        self.cosine_l_method_tt = list()

    def expand_csv_field_size_limit(self):
        dic = {0: list(), 1: list(), 2: list(), 3: list(), 4: list(),
               5: list(), 6: list(), 7: list(), 8: list(), 9: list()}
        maxInt = sys.maxsize
        while True:
            try:
                csv.field_size_limit(maxInt)
                break
            except OverflowError:
                maxInt = int(maxInt/10)

    def run(self, project):
        print('Running ', project)
        self.project_name = project
        self.expand_csv_field_size_limit()

        path_a = 'output/calculations/history_'+project+'.csv'
        project_list = cet_repository.get_db(path_a)

        path_b = 'output/calculations/average_'+project+'.csv'
        avgs = cet_repository.get_db(path_b)

        self.class_avgs = avgs[0]
        self.method_avgs = avgs[1]

        self.loop_project(project_list)

        self.normalize()

        self.save()

    def loop_project(self, project):
        for i, release_keys in enumerate(self.keys):
            #print('RELEASE; ', release_keys, ' i ', i)
            counter = 0
            for block in project:
                if block['type'] == 'class':
                    try:
                        self.is_class(block, i, release_keys)
                    except:
                        print('i', i, 'block', block)
                else:
                    # print(block)
                    self.is_method(block, i, release_keys)
            counter += 1

    def is_method(self, block, counter, keys):
        loc = 0
        if not block[keys[0]] == '':
            loc = int(block[keys[0]])

        outH = float(self.method_avgs['q95'])
        outL = float(self.method_avgs['q5'])
        if loc > outH or loc < outL:
            return
        if loc <= float(self.method_avgs['q25']):

            self.jaccard_xs_method[counter].append(float(block[keys[2]]))
            self.cosine_xs_method[counter].append(float(block[keys[3]]))
        elif loc <= float(self.method_avgs['q50']):
            self.jaccard_s_method[counter].append(float(block[keys[2]]))
            self.cosine_s_method[counter].append(float(block[keys[3]]))
        elif loc <= float(self.method_avgs['q75']):
            self.jaccard_m_method[counter].append(float(block[keys[2]]))
            self.cosine_m_method[counter].append(float(block[keys[3]]))
        elif loc <= float(self.method_avgs['q95']):
            self.jaccard_l_method[counter].append(float(block[keys[2]]))
            self.cosine_l_method[counter].append(float(block[keys[3]]))

    def is_class(self, block, counter, keys):
        loc = 0
        if not block[keys[0]] == '':
            loc = int(block[keys[0]])

        outH = float(self.class_avgs['q95'])
        outL = float(self.class_avgs['q5'])
        if loc > outH or loc < outL:
            return
        if loc <= float(self.class_avgs['q25']):
            self.jaccard_xs_class[counter].append(float(block[keys[2]]))
            self.cosine_xs_class[counter].append(float(block[keys[3]]))
        elif loc <= float(self.class_avgs['q50']):
            self.jaccard_s_class[counter].append(float(block[keys[2]]))
            self.cosine_s_class[counter].append(float(block[keys[3]]))
        elif loc <= float(self.class_avgs['q75']):
            self.jaccard_m_class[counter].append(float(block[keys[2]]))
            self.cosine_m_class[counter].append(float(block[keys[3]]))
        elif loc <= float(self.class_avgs['q95']):
            self.jaccard_l_class[counter].append(float(block[keys[2]]))
            self.cosine_l_class[counter].append(float(block[keys[3]]))

    def normalize(self):
        self.jaccard_xs_class_tt.extend(
            self.do_averages(self.jaccard_xs_class))
        self.jaccard_s_class_tt.extend(self.do_averages(self.jaccard_s_class))
        self.jaccard_m_class_tt.extend(self.do_averages(self.jaccard_m_class))
        self.jaccard_l_class_tt.extend(self.do_averages(self.jaccard_l_class))
        self.cosine_xs_class_tt.extend(self.do_averages(self.cosine_xs_class))
        self.cosine_s_class_tt.extend(self.do_averages(self.cosine_s_class))
        self.cosine_m_class_tt.extend(self.do_averages(self.cosine_m_class))
        self.cosine_l_class_tt.extend(self.do_averages(self.cosine_l_class))

        # print(self.jaccard_xs_method)
        self.jaccard_xs_method_tt.extend(
            self.do_averages(self.jaccard_xs_method))
        # print(self.jaccard_xs_method_tt)
        self.jaccard_s_method_tt.extend(
            self.do_averages(self.jaccard_s_method))
        self.jaccard_m_method_tt.extend(
            self.do_averages(self.jaccard_m_method))
        self.jaccard_l_method_tt.extend(
            self.do_averages(self.jaccard_l_method))
        # print(self.cosine_xs_method)
        self.cosine_xs_method_tt.extend(
            self.do_averages(self.cosine_xs_method))
        # print(self.cosine_xs_method_tt)
        self.cosine_s_method_tt.extend(self.do_averages(self.cosine_s_method))
        self.cosine_m_method_tt.extend(self.do_averages(self.cosine_m_method))
        self.cosine_l_method_tt.extend(self.do_averages(self.cosine_l_method))

    def do_averages(self, my_list):
        result = list()
        for key, value in my_list.items():
            result.append(self.average(value))
        return result

    def average(self, list):
        try:
            return sum(list)/len(list)
        except:
            return 0

    def save(self):
        path = 'output/calculations/final_'+self.project_name+'.csv'
        results = list()
        results.append(self.result_to_dict(
            self.jaccard_xs_class_tt, 'jaccard', 'q25', 'class'))
        results.append(self.result_to_dict(
            self.jaccard_s_class_tt, 'jaccard', 'q50', 'class'))
        results.append(self.result_to_dict(
            self.jaccard_m_class_tt, 'jaccard', 'q75', 'class'))
        results.append(self.result_to_dict(
            self.jaccard_l_class_tt, 'jaccard', 'q95', 'class'))

        results.append(self.result_to_dict(
            self.jaccard_xs_method_tt, 'jaccard', 'q25', 'method'))
        results.append(self.result_to_dict(
            self.jaccard_s_method_tt, 'jaccard', 'q50', 'method'))
        results.append(self.result_to_dict(
            self.jaccard_m_class_tt, 'jaccard', 'q75', 'method'))
        results.append(self.result_to_dict(
            self.jaccard_l_method_tt, 'jaccard', 'q95', 'method'))

        results.append(self.result_to_dict(
            self.cosine_xs_class_tt, 'cosine', 'q25', 'class'))
        results.append(self.result_to_dict(
            self.cosine_s_class_tt, 'cosine', 'q50', 'class'))
        results.append(self.result_to_dict(
            self.cosine_m_class_tt, 'cosine', 'q75', 'class'))
        results.append(self.result_to_dict(
            self.cosine_l_class_tt, 'cosine', 'q95', 'class'))

        results.append(self.result_to_dict(
            self.cosine_xs_method_tt, 'cosine', 'q25', 'method'))
        results.append(self.result_to_dict(
            self.cosine_s_method_tt, 'cosine', 'q50', 'method'))
        results.append(self.result_to_dict(
            self.cosine_m_method_tt, 'cosine', 'q75', 'method'))
        results.append(self.result_to_dict(
            self.cosine_l_method_tt, 'cosine', 'q95', 'method'))

        fields = ['project_name', 'cohesion_type', 'percentile', 'type', 'r:0',
                  'r:1', 'r:2', 'r:3', 'r:4', 'r:5', 'r:6', 'r:7', 'r:8', 'r:9']
        cet_repository.save_db(path, results, fields)

    def result_to_dict(self, my_list, ctype, size, btype):
        result = {'project_name': self.project_name,
                  'cohesion_type': ctype, 'percentile': size, 'type': btype}

        for i, release in enumerate(my_list):
            rel = 'r:'+str(i)
            result[rel] = release
        return result
