from lib import cet_repository as repository
from lib import cet_calc_documented
import sys
import csv


class calc:
    def __init__(self, args):
        super().__init__()
        self.project_args = args
        self.repositories = list()
        self.result = list()

    def expand_csv_field_size_limit(self):
        maxInt = sys.maxsize
        while True:
            # decrease the maxInt value by factor 10
            # as long as the OverflowError occurs.
            try:
                csv.field_size_limit(maxInt)
                break
            except OverflowError:
                maxInt = int(maxInt/10)

    def run(self):
        self.expand_csv_field_size_limit()
        print('Calculating')
        if self.get_repos():
            self.calculate()
            # return self.result

    def calculate(self):
        for project in self.repositories:
            m_calc_documented = cet_calc_documented.cet_calc_documented()
            #project_calculations = m_calc_documented.run(project)
            #print('PC:', project_calculations)
            m_calc_documented.run(project)

    def get_repos(self):
        self.repositories = repository.get_projects_packaged(self.project_args)
        if len(self.repositories) < 1:
            print('Repository not found')
            return False
        else:
            return True

    def print_array(self, array):
        for a in array:
            print(a)
            print('\n')
