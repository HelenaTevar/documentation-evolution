from lib import cet_repository
import csv
import sys


class variation_calc():
    def __init__(self):
        super().__init__()
        self.ratios = ['jaccard', 'cosine']
        self.perc = ['q25', 'q50', 'q75', 'q95']
        self.list = list()
        self.size_data = 15
        self.fields = ['project_name', 'cohesion_type', 'percentile', 'type', 'avg', 'r:0',
                       'r:1', 'r:2', 'r:3', 'r:4', 'r:5', 'r:6', 'r:7', 'r:8', 'r:9']

    def run(self, project):
        final_path = 'output/calculations/final_' + project + '.csv'
        data = cet_repository.get_db(final_path)
        self.calculate(data)
        save_path = 'output/calculations/variation_' + project + '.csv'
        cet_repository.save_db(save_path, self.list, self.fields)

    def calculate(self, data):
        index = 0

        while index <= self.size_data:
            average = list()

            item = data[index]
            result = {'project_name': item['project_name'],
                      'cohesion_type': item['cohesion_type'],
                      'percentile': item['percentile'],
                      'type': item['type'],
                      'avg': 0}
            key_num = 1

            result['r:0'] = 1.0
            average.append(result['r:0'])
            while key_num <= 9:
                key = 'r:'+str(key_num)
                key_m_1 = 'r:'+str(key_num-1)
                result[key] = self.division(item[key], item[key_m_1])
                average.append(result[key])
                key_num += 1
            result['avg'] = round(sum(average)/len(average), 4)
            print(result)
            self.list.append(result.copy())
            result.clear()
            index += 1

    def division(self, n, n_m_1):
        try:
            return float(n)/float(n_m_1)
        except:
            return 1.0
