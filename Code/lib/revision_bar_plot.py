import lib.cet_repository as cet_repository
import matplotlib
import matplotlib.pyplot as plt
import numpy as np


class revision_bar_plot():
    def __init__(self, projects):
        super().__init__()
        self.projects = projects
        self.jaccard_class = {'q25': 0.0, 'q50': 0.0, 'q75': 0.0, 'q95': 0.0}
        self.jaccard_method = {'q25': 0.0, 'q50': 0.0, 'q75': 0.0, 'q95': 0.0}
        self.cosine_class = {'q25': 0.0, 'q50': 0.0, 'q75': 0.0, 'q95': 0.0}
        self.cosine_method = {'q25': 0.0, 'q50': 0.0, 'q75': 0.0, 'q95': 0.0}

    def run(self):
        self.loop_projects()
        self.average_all()
        self.print_all()

    def get_results(self):
        results = [self.jaccard_class, self.jaccard_method,
                   self.cosine_class, self.cosine_method]
        return results

    def print_all(self):
        print("Jaccard Class: ", self.jaccard_class)
        print("Jaccard Method: ", self.jaccard_method)
        print("Cosine Class: ", self.cosine_class)
        print("Cosine Method: ", self.cosine_method)

    def average_all(self):
        self.jaccard_class = self.average(self.jaccard_class)
        self.jaccard_method = self.average(self.jaccard_method)
        self.cosine_class = self.average(self.cosine_class)
        self.cosine_method = self.average(self.cosine_method)

    def average(self, m_dic):
        result = m_dic.copy()
        for key, value in result.items():
            result[key] = round(value/10, 5)
        return result

    def loop_projects(self):
        for project in self.projects:
            self.read_one_project(project)

    def read_one_project(self, project):
        path = 'output/calculations/variation_' + project + '.csv'
        data_list = cet_repository.get_db(path)

        for data in data_list:
            if data['cohesion_type'] == 'jaccard':
                self.is_jaccard(data)
            else:
                self.is_cosine(data)

    def is_jaccard(self, data):
        if data['type'] == 'class':
            if data['percentile'] == 'q25':
                self.jaccard_class['q25'] = self.jaccard_class['q25'] + \
                    float(data['avg'])
            if data['percentile'] == 'q50':
                self.jaccard_class['q50'] = self.jaccard_class['q50'] + \
                    float(data['avg'])
            if data['percentile'] == 'q75':
                self.jaccard_class['q75'] = self.jaccard_class['q75'] + \
                    float(data['avg'])
            if data['percentile'] == 'q95':
                self.jaccard_class['q95'] = self.jaccard_class['q95'] + \
                    float(data['avg'])
        else:
            if data['percentile'] == 'q25':
                self.jaccard_method['q25'] = self.jaccard_method['q25'] + \
                    float(data['avg'])
            if data['percentile'] == 'q50':
                self.jaccard_method['q50'] = self.jaccard_method['q50'] + \
                    float(data['avg'])
            if data['percentile'] == 'q75':
                self.jaccard_method['q75'] = self.jaccard_method['q75'] + \
                    float(data['avg'])
            if data['percentile'] == 'q95':
                self.jaccard_method['q95'] = self.jaccard_method['q95'] + \
                    float(data['avg'])

    def is_cosine(self, data):
        if data['type'] == 'class':
            if data['percentile'] == 'q25':
                self.cosine_class['q25'] = self.cosine_class['q25'] + \
                    float(data['avg'])
            if data['percentile'] == 'q50':
                self.cosine_class['q50'] = self.cosine_class['q50'] + \
                    float(data['avg'])
            if data['percentile'] == 'q75':
                self.cosine_class['q75'] = self.cosine_class['q75'] + \
                    float(data['avg'])
            if data['percentile'] == 'q95':
                self.cosine_class['q95'] = self.cosine_class['q95'] + \
                    float(data['avg'])
        else:
            if data['percentile'] == 'q25':
                self.cosine_method['q25'] = self.cosine_method['q25'] + \
                    float(data['avg'])
            if data['percentile'] == 'q50':
                self.cosine_method['q50'] = self.cosine_method['q50'] + \
                    float(data['avg'])
            if data['percentile'] == 'q75':
                self.cosine_method['q75'] = self.cosine_method['q75'] + \
                    float(data['avg'])
            if data['percentile'] == 'q95':
                self.cosine_method['q95'] = self.cosine_method['q95'] + \
                    float(data['avg'])

    def plot_jaccard(self):
        labels = ['25', '50', '75', '95']
        classes = [self.jaccard_class['q25'], self.jaccard_class['q50'],
                   self.jaccard_class['q75'], self.jaccard_class['q95']]
        methods = [self.jaccard_method['q25'], self.jaccard_method['q50'],
                   self.jaccard_method['q75'], self.jaccard_method['q95']]

        x = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars

        fig, ax = plt.subplots()
        rects1 = ax.bar(x - width/2, classes, width, label='Classes')
        rects2 = ax.bar(x + width/2, methods, width, label='Methods')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Variation')
        ax.set_title('Jaccard Variation Ratio')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom', size=8)
        autolabel(rects1)
        autolabel(rects2)

        fig.tight_layout()
        plt.show()

    def plot_cosine(self):
        labels = ['25', '50', '75', '95']
        classes = [self.cosine_class['q25'],
                   self.cosine_class['q50'],
                   self.cosine_class['q75'],
                   self.cosine_class['q95']]

        methods = [self.cosine_method['q25'],
                   self.cosine_method['q50'],
                   self.cosine_method['q75'],
                   self.cosine_method['q95']]

        x = np.arange(len(labels))  # the label locations
        width = 0.35  # the width of the bars

        fig, ax = plt.subplots()
        rects1 = ax.bar(x - width/2, classes, width, label='Classes')
        rects2 = ax.bar(x + width/2, methods, width, label='Methods')

        # Add some text for labels, title and custom x-axis tick labels, etc.
        ax.set_ylabel('Variation')
        ax.set_title('Cosine Variation Ratio')
        ax.set_xticks(x)
        ax.set_xticklabels(labels)
        ax.legend()

        def autolabel(rects):
            """Attach a text label above each bar in *rects*, displaying its height."""
            for rect in rects:
                height = rect.get_height()
                ax.annotate('{}'.format(height),
                            xy=(rect.get_x() + rect.get_width() / 2, height),
                            xytext=(0, 3),  # 3 points vertical offset
                            textcoords="offset points",
                            ha='center', va='bottom', size=8)
        autolabel(rects1)
        autolabel(rects2)

        fig.tight_layout()

        plt.show()
