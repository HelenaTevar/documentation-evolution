from lib import cet_repository as repository
import numpy as np
import sys
import csv


class cet_statistics:
    def __init__(self):
        super().__init__()
        self.method_loc = list()
        self.class_loc = list()
        self.method_loc_historic = list()
        self.class_loc_historic = list()

        self.method_per = list()
        self.class_per = list()

        self.project_name = ''
        self.release_names = list()

        self.method_avg = {'q0': 0, 'q5': 0, 'q25': 0,
                           'q50': 0, 'q75': 0, 'q95': 0, 'q100': 0}
        self.class_avg = {'q0': 0, 'q5': 0, 'q25': 0,
                          'q50': 0, 'q75': 0, 'q95': 0, 'q100': 0}

    def expand_csv_field_size_limit(self):
        maxInt = sys.maxsize
        while True:
            # decrease the maxInt value by factor 10
            # as long as the OverflowError occurs.
            try:
                csv.field_size_limit(maxInt)
                break
            except OverflowError:
                maxInt = int(maxInt/10)

    def run(self, project):
        self.project_name = project[0]['project_name']
        print('Running average loc')
        for release in project:
            self.release_names.append(release['release_name'])
            self.calculateLOC(release)
            self.calculatePercentile()
            self.clean()
        self.calculateAverage()
        self.save()

    def calculateAverage(self):
        length = len(self.method_per)
        keys = ['q0', 'q5', 'q25', 'q50', 'q75', 'q95', 'q100']
        m = list()
        c = list()
        for key in keys:
            for d in self.method_per:
                m.append(d[key])
            avg = sum(m)/len(m)
            self.method_avg[key] = avg

            for d in self.class_per:
                c.append(d[key])
            avg = sum(c)/len(c)
            self.class_avg[key] = avg

            m.clear()
            c.clear()

    def clean(self):
        self.method_loc.clear()
        self.class_loc.clear()

    def calculatePercentile(self):
        result = '\nPercentile LOC Calculation'
        result += '\nProject: ' + self.project_name
        for x in range(len(self.release_names)):
            result += '\n\tRelease: ' + self.release_names[x]
            result += '\n\tBlock type: Method'
            pd = self.calcPercentile(self.method_loc)
            result += self.stringPercentile(pd)
            self.method_per.append(pd)

            result += '\n\tBlock type: Class'
            pd = self.calcPercentile(self.class_loc)
            result += self.stringPercentile(pd)
            self.class_per.append(pd)

    def stringPercentile(self, loc_dict):
        result = '\n\t\tQ0: ' + str(loc_dict['q0'])
        result += '\n\t\tQ5: ' + str(loc_dict['q5'])
        result += '\n\t\tQ25: ' + str(loc_dict['q25'])
        result += '\n\t\tQ50: ' + str(loc_dict['q50'])
        result += '\n\t\tQ75: ' + str(loc_dict['q75'])
        result += '\n\t\tQ95: ' + str(loc_dict['q95'])
        result += '\n\t\tQ100: ' + str(loc_dict['q100'])
        return result

    def calcPercentile(self, m_list):
        q5 = np.percentile(m_list, 5)
        q25 = np.percentile(m_list, 25)
        q50 = np.percentile(m_list, 50)
        q75 = np.percentile(m_list, 75)
        q95 = np.percentile(m_list, 95)
        q0 = np.percentile(m_list, 0)
        q100 = np.percentile(m_list, 100)

        result_dict = {'q0': q0, 'q5': q5, 'q25': q25,
                       'q50': q50, 'q75': q75, 'q95': q95, 'q100': q100}
        return result_dict

    def calculateLOC(self, release):
        release_blocks = repository.get_db(release['out'])
        for block in release_blocks:
            if block['type'] == 'method':
                self.method_loc.append(int(block['loc']))
            else:
                self.class_loc.append(int(block['loc']))
        self.class_loc_historic.append(self.class_loc.copy())
        self.method_loc_historic.append(self.method_loc.copy())

    def save(self):
        path = 'output/calculations/average_' + self.project_name + '.txt'
        print('Saving in: ', path)
        text_file = open(path, 'w')
        text_file.write(self.toString())
        text_file.close()
        self.save_to_dict()

    def save_to_dict(self):
        a = list()
        percentile_dict = dict()
        percentile_dict['project_name'] = self.project_name
        percentile_dict['type'] = 'class'
        percentile_dict['q0'] = self.class_avg['q0']
        percentile_dict['q5'] = self.class_avg['q5']
        percentile_dict['q25'] = self.class_avg['q25']
        percentile_dict['q50'] = self.class_avg['q50']
        percentile_dict['q75'] = self.class_avg['q75']
        percentile_dict['q95'] = self.class_avg['q95']
        percentile_dict['q100'] = self.class_avg['q100']
        a.append(percentile_dict.copy())

        percentile_dict['project_name'] = self.project_name
        percentile_dict['type'] = 'method'
        percentile_dict['q0'] = self.method_avg['q0']
        percentile_dict['q5'] = self.method_avg['q5']
        percentile_dict['q25'] = self.method_avg['q25']
        percentile_dict['q50'] = self.method_avg['q50']
        percentile_dict['q75'] = self.method_avg['q75']
        percentile_dict['q95'] = self.method_avg['q95']
        percentile_dict['q100'] = self.method_avg['q100']
        a.append(percentile_dict.copy())
        avg_path = 'output/calculations/average_'+self.project_name+'.csv'
        repository.save_db(avg_path, a, [
                           'project_name', 'type', 'q0', 'q5', 'q25', 'q50', 'q75', 'q95', 'q100'])

    def toString(self):

        result = 'ANALYSIS LOC'
        result += '\nProject: ' + self.project_name
        for x in range(len(self.release_names)):
            result += '\n\tRelease: ' + self.release_names[x]

            c = self.stringPercentile(self.class_per[x])
            result += '\n\t\tBlock type: Class'
            result += c

            m = self.stringPercentile(self.method_per[x])
            result += '\n\t\tBlock type: Method'
            result += m
            result += '\n'

        result += '\n\tAverage block type: Class'
        for key, value in self.class_avg.items():
            result += '\n\t\t' + key + ' -> ' + str(value)

        result += '\n'
        result += '\n\tAverage block type: Method'
        for key, value in self.method_avg.items():
            result += '\n\t\t' + key + ' -> ' + str(value)
        return result
