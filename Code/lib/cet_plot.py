import numpy as np
import matplotlib.pyplot as plt
import lib.cet_repository as cet_repository


'''
    ax.bar(labels, men_means, width, yerr=men_std, label='Men')
    ax.bar(labels, women_means, width, yerr=women_std, bottom=men_means,
          label='Women')

project_name,cohesion_type,percentile,r:0,r:1,r:2,r:3,r:4,r:5,r:6,r:7,r:8,r:9
'''


def plot_results(dict_list):
    x = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    a = dict_list[0]['results']
    b = dict_list[1]['results']
    c = dict_list[2]['results']
    d = dict_list[3]['results']
    # fig, ax = plt.subplots()  # Create a figure and an axes.
    fig, ax = plt.subplots(figsize=(5, 5))
    ax.plot(x, a, label=dict_list[0]['label'])  # Plot some data on the axes.
    ax.plot(x, b, label=dict_list[1]['label'])
    ax.plot(x, c, label=dict_list[2]['label'])
    ax.plot(x, d, label=dict_list[3]['label'])
    plt.xticks(x)
    ax.set_xlabel('Releases')  # Add an x-label to the axes.
    ax.set_ylabel(dict_list[3]['ylabel'])
    # Add a title to the axes.
    ax.set_title(dict_list[3]['title'])
    # Add a legend.
    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])

    ax.legend(loc='center left', bbox_to_anchor=(1, 0.5), )
    plt.tight_layout()
    plt.show()


def cohesion_plot(project):
    roundBy = 50
    #path = 'output/calculations/final_'+project+'.csv'
    path = 'output/calculations/variation_'+project+'.csv'
    data_list = cet_repository.get_db(path)
    result_to_plot_jaccard_c = list()
    result_to_plot_cosine_c = list()
    result_to_plot_jaccard_m = list()
    result_to_plot_cosine_m = list()
    for data in data_list:
        jd = {'label': data['percentile'], 'results': list(),
              'title': 'Evolution of ' + data['cohesion_type'] + ' ratio for ' + project + ' code blocks: ' + data['type']}
        jd['results'].append(round(float(data['r:0']), roundBy))
        jd['results'].append(round(float(data['r:1']), roundBy))
        jd['results'].append(round(float(data['r:2']), roundBy))
        jd['results'].append(round(float(data['r:3']), roundBy))
        jd['results'].append(round(float(data['r:4']), roundBy))
        jd['results'].append(round(float(data['r:5']), roundBy))
        jd['results'].append(round(float(data['r:6']), roundBy))
        jd['results'].append(round(float(data['r:7']), roundBy))
        jd['results'].append(round(float(data['r:8']), roundBy))
        jd['results'].append(round(float(data['r:9']), roundBy))
        jd['ylabel'] = data['cohesion_type']
        if data['cohesion_type'] == 'jaccard' and data['type'] == 'class':
            result_to_plot_jaccard_c.append(jd)
        elif data['cohesion_type'] == 'jaccard' and data['type'] == 'method':
            result_to_plot_jaccard_m.append(jd)
        elif data['cohesion_type'] == 'cosine' and data['type'] == 'class':
            result_to_plot_cosine_c.append(jd)
        else:
            result_to_plot_cosine_m.append(jd)

    plot_results(result_to_plot_jaccard_c)
    plot_results(result_to_plot_jaccard_m)
    plot_results(result_to_plot_cosine_c)
    plot_results(result_to_plot_cosine_m)


'''   
    x = np.linspace(0, 2, 100)

    # Note that even in the OO-style, we use `.pyplot.figure` to create the figure.
    fig, ax = plt.subplots()  # Create a figure and an axes.
    ax.plot(x, x, label='linear')  # Plot some data on the axes.
    ax.plot(x, x**2, label='quadratic')  # Plot more data on the axes...
    ax.plot(x, x**3, label='cubic')  # ... and some more.
    ax.set_xlabel('x label')  # Add an x-label to the axes.
    ax.set_ylabel('y label')  # Add a y-label to the axes.
    ax.set_title("Simple Plot")  # Add a title to the axes.
    ax.legend()  # Add a legend.
    plt.show()
'''


def stuff():
    labels = ['G1', 'G2', 'G3', 'G4', 'G5']
    men_means = [20, 35, 30, 35, 27]
    women_means = [25, 32, 34, 20, 25]
    men_std = [2, 3, 4, 1, 2]
    women_std = [3, 5, 2, 3, 3]
    width = 0.35       # the width of the bars: can also be len(x) sequence

    fig, ax = plt.subplots()

    ax.bar(labels, men_means, width, yerr=men_std, label='Men')
    ax.bar(labels, women_means, width, yerr=women_std, bottom=men_means,
           label='Women')

    ax.set_ylabel('Scores')
    ax.set_title('Scores by group and gender')
    ax.legend()

    plt.show()
