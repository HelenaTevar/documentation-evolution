import csv
import sys
from lib import cet_repository as repository
from lib import cet_calc_documented as documented_ratio_calc
from lib import cet_average_loc as average_loc_calc
from lib import cet_statistics_cohesion as cohesion_calc
from lib import cet_calc_history as history_calculator


class cet_analitics:
    def __init__(self, project):
        super().__init__()
        self.project_name = project
        self.project = self.get_project_data(project)

    def run(self):
        '''
        Runs cet_calculations for documetned and historics data.

        Runs cet_statistics for average loc calculations
        '''
        self.expand_csv_field_size_limit()
        self.calculate_documented_ratio()

        self.create_project_historic_file()
        self.calculate_average_loc()
        self.calculate_final_ratios()

    def calculate_variation(self):
        pass

    def calculate_documented_ratio(self):
        print('Calculating documented ratio for: ', self.project_name)
        print(self.project[0])
        #drc = documented_ratio_calc.cet_calc_documented()
        # drc.run(self.project)
        m_d = documented_ratio_calc.cet_calc_doc(self.project)
        m_d.run()
        pass

    def create_project_historic_file(self):
        print('Creating project historic files')
        history = history_calculator.calc_history(self.project)
        history.run()
        print('DONE')
        pass

    def calculate_average_loc(self):
        print('Calculating average loc')
        avg_loc = average_loc_calc.cet_statistics()
        avg_loc.run(self.project)
        pass

    def calculate_final_ratios(self):
        print('Calculating final ratios')
        final = cohesion_calc.statistics_cohesion()
        final.run(self.project_name)
        pass

    def expand_csv_field_size_limit(self):
        maxInt = sys.maxsize
        while True:
            # decrease the maxInt value by factor 10
            # as long as the OverflowError occurs.
            try:
                csv.field_size_limit(maxInt)
                break
            except OverflowError:
                maxInt = int(maxInt/10)

    def get_project_data(self, project):
        if project == 'all':
            print('TODO: fix this')
            exit(0)
        else:
            repo_resulted = repository.get_project(project)
            if len(repo_resulted) < 1:
                print('Project not found')
                exit(0)
            else:
                return repo_resulted
