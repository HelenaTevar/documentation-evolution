import lib.cet_filter as cet_filter
import re
import math
from collections import Counter


class cohesion_calculator():
    def __init__(self, block):
        super().__init__()
        self.block = block
        self.comment = ''
        self.content = ''
        self.pipe()
        self.jaccard = 0.0
        self.cosine = 0.0
        self.WORD = re.compile(r'\w+')

    def get_jaccard(self):
        self.calc_jaccard()
        return self.jaccard

    def get_cosine(self):
        self.calc_cosine()
        return self.cosine

    def calc_jaccard(self):
        comment_set = set(self.comment.split())
        content_set = set(self.content.split())
        intersection = comment_set.intersection(content_set)
        denominator = (len(comment_set) + len(content_set) - len(intersection))
        if not denominator:
            self.jaccard = 0.0
        else:
            result = float(len(intersection)) / (len(comment_set) +
                                                 len(content_set) - len(intersection))
            self.jaccard = result

    def calc_cosine(self):
        comment_vector = self.text_to_vector(self.comment)
        content_vector = self.text_to_vector(self.content)
        intersection = set(comment_vector.keys()) & set(content_vector.keys())
        numerator = sum([comment_vector[x]*content_vector[x]
                         for x in intersection])
        sum1 = sum([comment_vector[x] ** 2 for x in comment_vector.keys()])
        sum2 = sum([content_vector[x] ** 2 for x in content_vector.keys()])
        denominator = math.sqrt(sum1) * math.sqrt(sum2)
        if not denominator:
            self.cosine = 0.0
        else:
            result = float(numerator)/denominator
            self.cosine = result

    def pipe(self):
        '''
        Pipes the comments and the contents strings to normalized strings. 
        '''
        self.comment = self.pipe_comment()
        self.content = self.pipe_content()

    def pipe_comment(self):
        comment = ''
        for c in self.block.comments:
            comment += c
        return cet_filter.filter_comment(comment)

    def pipe_content(self):
        content = ''
        for c in self.block.block_content:
            content += c
        return cet_filter.filter_code(content)

    def text_to_vector(self, text):
        words = self.WORD.findall(text)
        return Counter(words)
