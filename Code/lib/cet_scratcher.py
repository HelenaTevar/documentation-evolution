from pathlib import Path
from lib import cet_tool, cet_block
import sys
import os


class cet_scratcher():
    '''
    Runs the extraction of java files by using cet_tool.

    Is used in main.py to start the program. 
    '''

    def __init__(self, release):
        super().__init__()
        self.release = release
        self.paths_list = list()
        self.path_copy = list()

    def run(self):
        '''
        Gets all the java files from the release['in'] address recursively.
        Iterates all the java files to run the cet_tool. 
        '''
        print('\nScratcher: Extraction started for ',
              self.release['release_name'])
        self.paths_list = self.recursive_java_files()
        self.iterate_paths()

    def recursive_java_files(self):
        return Path(self.release['in']).glob('**/*.java')

    def iterate_paths(self):
        print('Scratcher: Running tool for all java files  found')
        for path in self.paths_list:
            if not os.path.isdir(path):
                try:
                    java_file_lines = self.get_lines(path)
                    tool = cet_tool.cet_tool(
                        self.release, java_file_lines, self.get_string_path(path))
                    tool.run()
                except:
                    self.log_error(path)
                    print('Error in ', path)

        print('\nScratcher: Ended')

    def get_string_path(self, path):
        '''
        Builds a string with the general path of a java file, ignoring the release folder. 
        Is used to name the owners of blocks in cet_tool. It is required to ignore the release folder
        to be able to compare two blocks among all the releases.
        @return: string with file path
        '''
        b = str(path).split('/')
        result = ''
        b.pop(0)
        b.pop(0)
        b.pop(0)
        result = '/'.join(b)
        return result

    def get_lines(self, path):
        try:
            with open(path, 'r') as java_file:
                return java_file.readlines()
        except:
            print('Scratcher: this path does not point to a java file - ', path)

    def log_error(self, path):
        line = 'Error with file: ' + str(path) + '\n'
        with open('output/error.txt', 'a') as my_error_file:
            my_error_file.write(line)
        print(line)
