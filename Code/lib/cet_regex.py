def get_comments_regex_str():
    # return '^(\s*)(\*)|(/\*\s*\*)|(/\s*/)|(/\s*\*)'
    # return '^((\s*)(\*)|(/\*\s*\*)|(/\s*/)|(/\s*\*)).*'
    #    return '^\s*((/(\s*)\*(\s*)\*)|(/(\s*)\*)|(/(\s*)/)|(\*)).*'
    # return '^\s*(\*\s*/)|(((/(\s*)\*(\s*)\*)|(/(\s*)\*)|(/(\s*)/)|(\*)).*$)'
    return '^\s*((\*\s*/)+|(/\s*\*)|(/\s*\*\s*\*)|(\*\s*)|(/\s*/)).*'


def get_comments_blocks():
    return '^(\s*)(\*)|(/\*\s*\*)|(/\s*\*)'


def get_closing_comments():
    return '\*/|//'


def get_class_regex():
    return '^\s*(Annotation\s*|public\s*|protected\s*|private\s*|static\s*|abstract\s*|final\s*|native\s*|synchronized\s*|transient\s*|volatile\s*|strictfp\s*)*' + \
        '(class|interface|enum)' + '\s*(?P<id>[a-zA-Z_0-9]+)'


def get_method_regex():
    # return '(.+\s|\s)(?P<id>.*\s*\(.*\))(\s*{)'
    # return '(.+\s|\s)(?P<id>.*\s*\(.*\))'
    # return '(\w+\s|\s)(?P<id>\w*\s*\(\w*\))(\s+\{)?'
    # return '(\w+\s|\s)*(?P<id>\w+\s*\((\s|\w)*\))(\s+\{)?.*'
    # return '\s*(\w+\s)*(?P<id>\w+\s*\(\s*(\w+\s\w+(\s\w+)*)\s*(,\s*\w+\s\w+(\s\w+)*)*\s*\))\s*(\{)?.*'
    # return '\s*(\w+\s+)*(?P<id>\w+\s*\((\s*(\w+(\s+\w+)+)(\s*,\s*\w+(\s+\w+)+)*)*\s*\)).*'
    # return '\s*((\w|<|>)+\s+)*(?P<id>(\w|<|>)+\s*\((\s*((\w|<|>)+(\s+(\w|<|>)+)+)(\s*,\s*(\w|<|>)+(\s+\w+)+)*)*\s*\)).*'
    # return '.*(?P<id>\s+\w+\s*\(([a-zA-Z_?<>,.\s]+|\s*)\)).*'
    # return '(\s*[a-zA-Z0-9<>,]+)*(?P<id>(\s+\w+\()(\s*[a-zA-Z0-9,.<>?]+\s*)*\)).*'
    # return '(\s*[a-zA-Z0-9_?.,<>]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\((\s*[a-zA-Z0-9_?.,<>]+)*\s*\))\s*.*'
    # return '(\s*[a-zA-Z0-9_?.,<>\[\]]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\((\s*[a-zA-Z0-9_?.,<>\[\]]+)*\s*\))\s*.*'
    # return '(\s*[a-zA-Z0-9_?.,<>\[\]]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\((\s*[a-zA-Z0-9_?.,<>@\[\]]+)*\s*\))\s*.*'
    # return '(\s*[a-zA-Z0-9_?.,<>\[\]]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\(((\s*[a-zA-Z0-9_?.,<>@\[\]]+)\s*(@\w+\s*\(\s*\"\s*\w+\s*\"\s*\))?)*\s*\))\s*.*
    # return '(\s*[a-zA-Z0-9_?.,<>\[\]]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\(((\s*[a-zA-Z0-9_?.,<>@\[\]]+)\s*(@\w+\s*\(\s*\"\s*\w+\s*\"\s*\))?)*\s*\))\s*.*'
    # return '(\s*[a-zA-Z0-9_?.,<>\[\]]+\s+)*(?P<id>\s?[a-zA-Z0-9_?.,<>]+\s*\(((\s*[a-zA-Z0-9_?.,<>\[\]]+)*\s*(@\w+\s*\(\s*\"\s*\w+\s*\"\s*\))?)*\s*\))\s*.*'
    # return '^\s*([a-zA-Z0-9_<>]+\s+)*(?P<id>[a-zA-Z0-9_<>]+\s*\((\s*[a-zA-Z0-9?@()\"_<>,.]+)*\s*\))(.*)'
    # return '^\s*([a-zA-Z0-9,._<>]+\s+)*(?P<id>[a-zA-Z0-9_<>]+\s*\((\s*[a-zA-Z0-9?@()\"_\[\]<>,.]+)*\s*\))(.*)'
    return '^([a-zA-Z0-9?,._<>\[\]]+\s+)*(?P<id>[a-zA-Z0-9_<>]+\s*\((\s*[a-zA-Z0-9?@()\"_\[\]<>,.]+)*\s*\))(.*)'


def get_possible_method_regex():
    # return '\s*(.*)(\(.*\))(\s*{)'
    # return '\s*(.*)(\(.*\))(\s*)'
    # return '\s*(.*)(\(.*\))'
    # return '\s*(.*)(\(.*\))(\s*throws\s+\w*)?(\s*{\s*)?$'
    # return '^\s*([a-zA-Z0-9_?.,<>\[\]]+\s+)*(\(.*\))(\s*throws\s+\w*)?(\s*{\s*)?$'
    # return '(^(\s*[a-zA-Z0-9_?.,<>\[\]]+)+\s*)\(.*\).*'
    # return '\s*(.*\w+.*)(\(.*\))(\s*throws\s+\w*)?(\s*{\s*)?$'
    # return '\s*(.*\w+.*)(\(.*\)).*'
    return '(\w?[a-zA-Z0-9_?.,<>\[\]]+\s*)+(\(.*\)).*'


def get_conditionals_regex():
    '''
    Matches all the possibilities of java functions that may look like a method, but are not methods.
    For instance if conditionals look like methods.
    '''
    # return '\s*(if|while|for|do|try|switch|catch).*'
    # return '\s*(if|while|for|do|try|switch|catch|@).*'
    # return '\s*(!|&|\||<|>|=|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\().*'
    # return '\s*(&|!|synchronized|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\(|((/)?(\*){1,2})).*'

    # return '\s*(super|synchronized|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\(|!|&|\||<|>|=).*'
    # return '^\s*(\"|\'|/|super|synchronized|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\(|!|&|\||<|>|=).*'
    # return '^\s*(\}|\"|\'|/|super|synchronized|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\(|!|&|\||<|>|=).*'
    # return '^\s*(\}|\"|\'|/|throw|super|synchronized|default|case|return|if|else|while|for|do|try|switch|catch|@|.*;$|.*:$|.*\..*\(|!|&|\||<|>|=).*|.*=.*|\s*\(\)\s*->.*'
    # return '^\s*(\)|\(|\{|\}|\/|\*|!|\&|<|>|\||@)*\s*((throw|super|synchronized|default|case|return|if|else|while|for|do|try|catch|switch).*)*\s*(.*\..*|.*=.*|.*->.*|.*;$)?\s*'
    s = '^(\)|\(|\{|\}|\/|\*|!|\&|<|>|\||@|\.)'
    c = '^(throw|super|synchronized|default|case|return|if|else|while|for|do|try|catch|switch)'
    #p = '.*;$'

    i = s + '|' + c
    return i


def get_whitespace_regex():
    return '\s\s+'


def get_filter_comment():
    return '/\*\*|/\*|//|\*\*\s+|\*/|\*\s'


def get_filter_java_keywords():
    return '(^|\s)(ArrayList|LinkedList|true|false|abstract|assert|boolean|break|byte|case|catch|char|class|const|continue|default|do|double|else|enum|extends|final|finally|float|for|goto|if|implements|import|instanceof|interface|int|long|native|new|package|private|protected|public|return|short|static|strictfp|super|switch|synchronized|this|throw|throws|transient|try|void|volatile|while)'


def get_filter_java_keysymbols():
    return '(=|\+|-|\(|\)|{|}|\.|<|>|;)'


def get_filter_word():
    return '\w+'


def get_numbers_regex():
    return '[0-9]'


def get_pronoms():
    return '(^|\s)(I|me|my|mine|myself|your|yours|you|yourself|he|him|his|himself|she|hers|her|herself|its|it|itself|we|us|ours|our|ourselves|yours|your|you|yourselves|they|them|theirs|their|themselves)\s+'


def get_prepositions():
    return '\s(a|and|aboard|about|above|according|across|after|against|along|alongside|amid|anti|around|as|aside|at|atop|because|before|behind|below|beneath|beside|besides|between|beyond|but|by|concerning|considering|despite|down|during|excepting|except|excluding|following|for|from|in|inside|instead|into|like|minus|near|off|of|onto|on|opposite|outside|out|over|past|per|plus|prior|regarding|round|save|since|than|through|throughout|till|toward|towards|to|under|underneath|unlike|until|until|upon|up|versus|via|within|without|with)\s'


def get_separators():
    return '_|-|,|\.|\*|\+|='


def get_naming_conventions_regex():
    return '(?<=[a-z]|[0-9])(?=[A-Z])|(?<=[A-Z])(?=[A-Z][a-z]|[0-9])'
