import re
from lib import cet_regex, cet_cohesion, cet_repository
from lib import cet_block as cet_block


class cet_tool():
    '''
    Reads all the lines of a java file, extracts blocks and calculates  cohesion ratios.
    @return: Writes results in csv file for project release.
    '''

    def __init__(self, project_data, lines, path):
        super().__init__()
        self.release = project_data
        self.lines = lines
        self.path = path
        self.depth = 0
        self.block_list = list()
        self.to_save_list = list()
        self.regex_ws = re.compile('\s*|')
        self.regex_comment = re.compile(cet_regex.get_comments_regex_str())
        self.regex_comment_block = re.compile(cet_regex.get_comments_blocks())
        self.regex_method = re.compile(cet_regex.get_method_regex())
        self.regex_class = re.compile(cet_regex.get_class_regex())
        self.regex_closed_comment = re.compile(
            cet_regex.get_closing_comments())

    def run(self):
        kickstart = cet_block.cet_block(self.release)
        kickstart.depth = self.depth
        print('\t Tool: Reading java file')
        self.block_list.append(kickstart)
        for line in self.lines:
            stripped_line = line.strip()
            self.run_extraction(stripped_line)

        self.calculate()

        self.save()
        print('\t Tool: Finishing java file\t')
        return

    def calculate(self):
        '''
        Calculates the cohesion ratios for the blocks in self.to_save_list
        '''
        for block in self.to_save_list:
            calculator = cet_cohesion.cohesion_calculator(block)
            block.jaccard = calculator.get_jaccard()
            block.cosine = calculator.get_cosine()
            block.comments_filtered = calculator.comment
            block.content_filtered = calculator.content

    def run_extraction(self, line):
        '''
        Routes lines to class, comment, method or other process.
        '''
        if self.is_class(line):
            self.set_class(line)
            self.check_brackets(line)

        elif self.is_comment(line):
            self.add_comment(line)

        elif self.is_method(line):
            self.set_method(line)
            self.check_brackets(line)

        else:
            self.check_brackets(line)

    def check_brackets(self, line):
        '''
        Adds or removes depth depending on open or closed brackets.
        When removing depth, saves closed blocks
        '''
        self.check_block_content(line)
        if '{' in line:
            self.add_depth()

        if '}' in line:
            self.remove_depth()
            self.block_list = self.filter_closed_blocks()

    def check_block_content(self, line):
        '''
        Adds line of code to all the blocks that are declared, if the block is not declared, clear the comments. 

        Example:


        'class A'  -> block declared

        '{ '-> Add new block NoName (not declared)

        '    //inline '-> inline saved in block[-1]

        '    return; '-> Block class A is declared, add to contents

                    -> Block NoName is not declared, delete comment.
        '''
        for block in self.block_list:
            if not block.is_empty():
                block.block_content.append(line)
                block.loc += 1
            else:
                block.comments.clear()

    def add_loc(self):
        for block in self.block_list:
            block.loc += 1

    def filter_closed_blocks(self):
        '''
        Saves closed and declared blocks.

        @return: list with opened blocks.
        '''
        resulted = list()
        for block in self.block_list:
            if block.depth >= self.depth and block.identifier != None:
                self.to_save_list.append(block)
            elif block.depth < self.depth:
                resulted.append(block)
            elif block.depth >= self.depth and block.identifier == None:
                pass
        kickend = cet_block.cet_block(self.release)
        kickend.depth = self.depth
        resulted.append(kickend)
        return resulted

    def add_depth(self):
        '''
        self.depth += 1
        self.block_list.append(new block)
        '''
        self.depth += 1
        to_add = cet_block.cet_block(self.release)
        to_add.depth = self.depth
        self.block_list.append(to_add)

    def remove_depth(self):
        self.depth -= 1

    def add_comment(self, line):
        self.block_list[-1].comments.append(line)

    def method_clean_up(self, line):
        '''
        Trims all the excesive information before and after the 
        method identificator and parameters. In those cases that
        the string is a failse positive, it will return an empty value.

        Example: 

        I > public void example(int a) // a comment

        O > example(int a)

        @return: string with method or nothing. 
        '''
        def trimEnd(line_stripped):
            buff = ''
            i = 0
            for c in reversed(line_stripped):
                if c != ')':
                    buff = c + buff
                else:
                    buff = c + buff
                    break

            if buff:
                i = line_stripped.index(buff)
                line_stripped = line_stripped[:i+1]
            return line_stripped

        def trimStart(line_stripped):
            i = 0
            for c in line_stripped:
                if c == '(':
                    i = line_stripped.index(c)
                    break

            modifiersAndId = line_stripped[:i]

            arrayMethod = modifiersAndId.split()
            if '.' in arrayMethod[-1]:
                result = ''
            else:
                i = line_stripped.index(arrayMethod[-1])
                result = line_stripped[i:]
            return result

        line_stripped = line.strip()
        result = line_stripped

        if '//' in line:
            i = line_stripped.index('//')
            line_stripped = line_stripped[:i]
        end_clean = trimEnd(line_stripped)

        result = trimStart(end_clean)

        return result

    def set_method(self, line):
        line_clean = self.method_clean_up(line)
        if line_clean:
            self.block_list[-1].type = 'method'
            owner = ''
            iden = line_clean
            self.block_list[-1].identifier = iden
            for block in reversed(self.block_list):
                if block.type == 'class':
                    owner = block.identifier
                    break
            self.block_list[-1].owner = owner

    def set_class(self, line):
        self.block_list[-1].type = 'class'
        regex_result = self.regex_class.search(line)
        identifier = regex_result.group('id').strip(' \t\n\r')
        ide = identifier + ':' + str(self.path)
        self.block_list[-1].identifier = ide
        self.block_list[-1].owner = ide

    def add_block_content(self, line):
        for block in self.block_list:
            block.block_content.append(line)

    # Booleans

    def is_class(self, line):
        return self.regex_class.search(line)

    def is_method(self, line):
        ''' 
        @return: boolean True if is a method declaration
        '''
        endswith = [',', '(', '\'', '\"']
        inside = [';', '=', '->', '+']
        startswith = ['}', '\"', '\'', '@', '?', '|', '&', '+', '.', 'this', 'assert', 'new', 'throw', 'super', 'synchronized', 'default', 'case',
                      'return', 'if', 'else', 'while', 'for', 'do', 'catch', 'try', 'switch']
        if '(' in line and ')' in line:
            for i in inside:
                if i in line:
                    return False
            for word in startswith:
                if line.startswith(word):
                    return False
            for word in endswith:
                if line.endswith(word):
                    return False
            return True
        else:
            return False

    def is_comment(self, line):
        '''
        Checks if a line is a comment or if is included in a block of comments.
        '''
        if self.regex_comment.search(line):
            return True
        elif len(self.block_list[-1].comments) == 0:
            return False
        elif self.block_list[-1].identifier != None:
            return False
        elif self.regex_closed_comment.search(self.block_list[-1].comments[-1]):
            return False
        else:
            return True

    def is_open_bracket(self, line):

        return self.regex_open_bracket.search(line)

    def is_closed_bracket(self, line):

        return self.regex_closed_bracket.search(line)

    def save(self):
        '''
        Saves the list to_save_list into the release csv file.
        '''
        dict_list = []
        for block in self.to_save_list:
            m_dic = block.to_dict()
            dict_list.append(m_dic)
        cet_repository.save_results(self.release, dict_list)

    def print_to_save_list(self):
        ##print('=== To Save List ===')
        for block in self.to_save_list:
            block.print_verbose()
