# Thesis
Tool used for Thesis: Documentation Evolution

By: Helena Tevar

## Adding repositories
Add folders with repositories by project  in the input directory. 
For instance: 

```
thesis
│   main.py
│
└───input
│   │
│   └───maven
│   |   │   release 1
│   |   │   release 2
│   |   └───...
|   |  
│   └───tomcat
│       │   release 1
│       │   release 2
│       └───...
│   
└───lib
│   │   projects.csv
│   └───...
│   
└───output
```

Create a list of your repositories in `lib.projects.csv` with this information per row: `project_name,release_name,release_date,in,out`
For example: 
```csv
project_name,release_name,release_date,in,out
test, test1,01 01 2001,input/test/test1,output/test/t1.csv
myProject,v.1,01 01 2001,input/myProject/v.1,output/myProject/v.1.csv
myProject,v.2,02 02 2002,input/myProject/v.2,output/myProject/v.2.csv
myProject,v.3,03 03 2003,input/myProject/v.3,output/myProject/v.3.csv
```

After running the program, the scratch results will be  saved in the output directory. 

```
thesis
│   main.py
│
└───input
│   │
│   └───maven
│       │   release 1
│       │   release 2
│       │   ...
│   └───tomcat
│       │   release 1
│       │   release 2
│       └───...
│   
└───lib
│   │   projects.csv
│   └───...
│   
└───output
│   │
│   └───maven
│       │   release 1
│       │   release 2
│       └───...
│   └───tomcat
│       │   release 1
│       │   release 2
│       └───...
│
```

## Scratcher
You need to install the package nltk  to run this programm. 
`sudo pip3 install -U nltk`
You may require to complete the instalation of nltk manually with 

```python
python3

import nltk
nltk.install('stopwords')
```

After nltk installation please run the main application with 'all'  as argument to run the scratcher for all the repositories defined in projects.csv or 'my_project' for a specific running. 

```python
python3 main.py all
```

After running the program, the console will output something like this:  

```
Main: Running
Repo: Checking output directories -  output/test
Main: Running -   test1

Extraction started for:   test1
Scratcher: Running tool for all java files  found
.....
Scratcher: Ended
Main: Finished -   test1
Main: FINISHED - Please check the output folder to get the results
```	

## main.py
The files used for the main.py application are: 
- cet_scratcher.py
- cet_tool.py
- cet_regex.py
- cet_cohesion.py
- cet_filter.py
- cet_repository.py
- cet_block.py (data structure)

## main_statistics.py
Run main_statistics with the project name to run statistics. Output in otput/calculations
Files used in main_statistics.py
- cet_analitics.py
- cet_average_loc.py
- cet_calc_documented.py
- cet_calc.py
- cet_statistics_cohesion.py

## main_plot.py 
Run main_plot with the project name to plot statistics.
- main_plot.py
- cet_plot.py




Jaccard Class:  {'q25': 1.1040000000000003, 'q50': 1.1000100000000002, 'q75': 1.1030200000000003, 'q95': 1.10146}
Jaccard Method:  {'q25': 1.0986, 'q50': 1.09919, 'q75': 1.1030200000000003, 'q95': 1.09737}
Cosine Class:  {'q25': 1.1027799999999999, 'q50': 1.09922, 'q75': 1.1029, 'q95': 1.10295}
Cosine Method:  {'q25': 1.0987200000000001, 'q50': 1.09873, 'q75': 1.09804, 'q95': 1.0973}