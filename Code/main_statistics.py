import matplotlib.pyplot as plt
import numpy as np
import lib.cet_plot as cet_plot
import lib.cet_analitics as analitics
import lib.revision_plot as revision
import lib.revision_bar_plot as bar_plot
project_names = ['che', 'cxf', 'elasticsearch', 'graal',
                 'guava', 'jmeter', 'maven', 'netbeans', 'springboot', 'tomcat']
for project in project_names:
    tool = revision.revision_plot(project)
    tool.run()

foo = bar_plot.revision_bar_plot(project_names)
foo.run()
foo.plot_jaccard()
foo.plot_cosine()

# ----------------------------------------------------
# cet_plot.cohesion_plot('maven')
# cet_plot.plot_documented_stacked('maven')


#a = analitics.cet_analitics('maven')
# a.run()

#from lib import cet_calc_variation as variation_calc
#variation = variation_calc.variation_calc()

# variation.run('tomcat')
