import re
import lib.cet_regex as cet_regex
import regex


s = '(^\s*(\(|\)|\{|\}|<|>|@|\||\*|!|\/|\.))'
c = '(^\s*(throw|super|synchronized|default|case|return|if|else|while|for|do|try|catch|switch)(.*|\s*)+)'
p = '.*(=|->|;$).*'
i = s + '|' + c + '|' + p


regex_method = re.compile(cet_regex.get_method_regex())
regex_possible_method = re.compile(
    cet_regex.get_possible_method_regex())
regex_conditionals = re.compile(cet_regex.get_conditionals_regex())


regex_check = re.compile(i)
lines = ['     system.out.print(\'Hello World\')',
         '                       .setMessage( "Unresolveable build extension: " + e.getMessage() )',
         '    public Map<String, List<String>> getInjectedProfileIds()',
         ' if ( foo )',
         '       public Logger getLogger( String name ) ',
         'public void merge( Model target, Model source, boolean sourceDominant, Map<?, ?> hints )',
         ' short()',
         '   foo().foo = bar ',
         'public boolean isDebugEnabled() // (ie foo)',
         'protected String getLocation( final StackTraceElement e )',
         'public void setParameters( List<Parameter> parameters )',
         'protected static <T> boolean eq( T s1, T s2 )',
         'public boolean canConvert(@SuppressWarnings("rawtypes") Class arg0) {']


def do_something1():
    for line in lines:

        print('---------------')
        print(line)
        if regex_possible_method.search(line):
            print('Method??')
        if regex_check.search(line):
            print('THE LINE MUST DIE ')
        elif regex_method.search(line):
            result = regex_method.search(line)
            id = result.group('id')
            print('id:', id)


s = '^(\)|\(|\{|\}|\/|\*|!|\&|<|>|\||@|\.)'
c = '^(throw|super|synchronized|default|case|return|if|else|while|for|do|try|catch|switch)'
p = '.*;$'

i = s + '|' + c + '|' + p
rs = re.compile(s)
rc = re.compile(c)
rp = re.compile(p)


def do_something2():
    for line in lines:
        print('---------------')
        l = line.strip()
        print(l)
        if regex.search(cet_regex.get_possible_method_regex(), l):
            print('Method??')
        if rs.search(l):
            print('RSymbols ON')
        if rc.search(l):
            print('RCondicionals ON')
        if rp.search(l):
            print('RPATTERN on')
        elif regex.search(cet_regex.get_method_regex(), l):
            result = regex_method.search(l)
            id = result.group('id')
            print('id:', id)


do_something2()
